class Pet:
    def __init__(self, species=None, name=""):
        self.name = name
        self.reference = ['dog', 'cat', 'horse', 'hamster']
        if species.lower() not in self.reference:
            raise ValueError(f"Species must be within this list {self.reference}")
        else:
            self.species = species

    def __str__(self):
        if len(self.name) == 0:
            return f"Species of: {self.species}, unnamed"
        else:
            return f"Species of: {self.species}, named {self.name}"


class Dog(Pet):
    def __init__(self, name="", chases="Cats"):
        super().__init__("Dog", name)
        self.name = name
        self.chases = chases

    def __str__(self):
        if len(self.name) == 0:
            return f"Species of Dog, unnamed, chases {self.chases}"
        else:
            return f"Species of Dog, named {self.name}, chases {self.chases}"


class Cat(Pet):
    def __init__(self, name="", hates="Dogs"):
        super().__init__("Cat", name)
        self.name = name
        self.hates = hates

    def __str__(self):
        if len(self.name) == 0:
            return f"Species of Dog, unnamed, chases {self.hates}"
        else:
            return f"Species of Dog, named {self.name}, chases {self.hates}"


class Main(Dog, Cat):
    def __init__(self, Species, name=""):
        super().__init__(name)
        if Species == "Dog":
            Dog.__init__(name)
        if Species == "Cat":
            Cat.__init__(name)


result = {"Cat":[],"Dog":[]}
dog_count = 0
flag = 0
cat_count = 0
while True:
    spe = input("Enter the species of Pet:")
    na = input("Enter the name of Pet:")

    if cat_count <= 3:
        if spe == "Cat":
            result["Cat"].append(Main(spe, na))
            cat_count += 1
            continue
        else:
            flag = 1

    if dog_count <= 5:
        if spe == "Dog":
            result["Dog"].append(Main(spe, na))
            dog_count+=1
            continue
        else:
            flag = 1






